// Generated from Calc.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CalcLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, PLUS=4, MINUS=5, MULT=6, DIV=7, ASSIGN=8, INTEGER=9, 
		IDENTIFIER=10, NUMBER=11, WHITESPACE=12, DIGIT=13, LETTER=14, LOWER=15, 
		UPPER=16;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "PLUS", "MINUS", "MULT", "DIV", "ASSIGN", "INTEGER", 
		"IDENTIFIER", "NUMBER", "WHITESPACE", "DIGIT", "LETTER", "LOWER", "UPPER"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "'('", "')'", "'+'", "'-'", "'*'", "'/'", "'='", "'integer'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, "PLUS", "MINUS", "MULT", "DIV", "ASSIGN", "INTEGER", 
		"IDENTIFIER", "NUMBER", "WHITESPACE", "DIGIT", "LETTER", "LOWER", "UPPER"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public CalcLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Calc.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\22Y\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\3"+
		"\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\13\3\13\3\13\7\13?\n\13\f\13\16\13B\13\13\3\f\6\fE"+
		"\n\f\r\f\16\fF\3\r\6\rJ\n\r\r\r\16\rK\3\r\3\r\3\16\3\16\3\17\3\17\5\17"+
		"T\n\17\3\20\3\20\3\21\3\21\2\2\22\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22\3\2\3\5\2\13\f\17\17\"\"]\2"+
		"\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2"+
		"\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2"+
		"\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\3#\3\2\2"+
		"\2\5%\3\2\2\2\7\'\3\2\2\2\t)\3\2\2\2\13+\3\2\2\2\r-\3\2\2\2\17/\3\2\2"+
		"\2\21\61\3\2\2\2\23\63\3\2\2\2\25;\3\2\2\2\27D\3\2\2\2\31I\3\2\2\2\33"+
		"O\3\2\2\2\35S\3\2\2\2\37U\3\2\2\2!W\3\2\2\2#$\7=\2\2$\4\3\2\2\2%&\7*\2"+
		"\2&\6\3\2\2\2\'(\7+\2\2(\b\3\2\2\2)*\7-\2\2*\n\3\2\2\2+,\7/\2\2,\f\3\2"+
		"\2\2-.\7,\2\2.\16\3\2\2\2/\60\7\61\2\2\60\20\3\2\2\2\61\62\7?\2\2\62\22"+
		"\3\2\2\2\63\64\7k\2\2\64\65\7p\2\2\65\66\7v\2\2\66\67\7g\2\2\678\7i\2"+
		"\289\7g\2\29:\7t\2\2:\24\3\2\2\2;@\5\35\17\2<?\5\35\17\2=?\5\33\16\2>"+
		"<\3\2\2\2>=\3\2\2\2?B\3\2\2\2@>\3\2\2\2@A\3\2\2\2A\26\3\2\2\2B@\3\2\2"+
		"\2CE\5\33\16\2DC\3\2\2\2EF\3\2\2\2FD\3\2\2\2FG\3\2\2\2G\30\3\2\2\2HJ\t"+
		"\2\2\2IH\3\2\2\2JK\3\2\2\2KI\3\2\2\2KL\3\2\2\2LM\3\2\2\2MN\b\r\2\2N\32"+
		"\3\2\2\2OP\4\62;\2P\34\3\2\2\2QT\5\37\20\2RT\5!\21\2SQ\3\2\2\2SR\3\2\2"+
		"\2T\36\3\2\2\2UV\4c|\2V \3\2\2\2WX\4C\\\2X\"\3\2\2\2\b\2>@FKS\3\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}