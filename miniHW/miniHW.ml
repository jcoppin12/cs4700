let random = 17 ;;
let pi = 3.14159 ;;
let myFirstFun n  =
        (n+3) * 4 ;;
let circumference r = 
         2. *. r *. pi ;;
let double n = (n, 2*n) ;;
let make_bigger x =
        if x<0.0 then x*.(-1.0)
        else if x>0.0 && x<1.0 then x+.0.5
        else x*.x ;;
